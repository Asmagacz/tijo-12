package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Component
public class MovieReaderImplementation implements MovieReader {

    @Bean
    public List<MovieDto> readFromCSV() {
        String[] lines;

        List<MovieDto> movies = new ArrayList<MovieDto>();
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        try {
            br = new BufferedReader(new FileReader("movies.csv"));

            while ((line = br.readLine()) != null) {
                lines = line.split(cvsSplitBy);
                movies.add(new MovieDto(lines[0], lines[1], lines[2], lines[3]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        movies.remove(0);
        return movies;
    }
}
