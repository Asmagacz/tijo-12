package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;

import java.util.List;

public interface MovieReader {
    public List<MovieDto> readFromCSV();
}
